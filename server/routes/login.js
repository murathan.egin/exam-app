const express = require('express');
const router = express.Router();
const pkceChallenge = require('pkce-challenge');

router.get('/', (req, res) => {
  //generate the pkce challenge/verifier dict
  pkce_pair = pkceChallenge();
  // Store the PKCE verifier in session
  req.session.verifier = pkce_pair['code_verifier']
  const stateValue = Math.random().toString(36).substring(2,15) + Math.random().toString(36).substring(2,15) + Math.random().toString(36).substring(2,15) + Math.random().toString(36).substring(2,15) + Math.random().toString(36).substring(2,15) + Math.random().toString(36).substring(2,15);
  req.session.stateValue = stateValue
  res.redirect(`http://localhost:9011/oauth2/authorize?client_id=9fa55346-09d7-4d4d-962b-852dc41c7e4b&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%3A9000%2Foauth-callback`);
});

module.exports = router;