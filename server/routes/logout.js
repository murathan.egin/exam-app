const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
  // delete the session
  req.session.destroy();

  // end FusionAuth session
  res.redirect(`http://localhost:9011/oauth2/logout?client_id=9fa55346-09d7-4d4d-962b-852dc41c7e4b`);
});

module.exports = router;
