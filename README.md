VUE.js and Express server Application

///// A Short Description of Application

Application aims to implement OAuth2.0 with Fusion Auth using Vue.js. The client will access on Port 8081, and will face login webpage. After successfully entered login credentials, server will respond to redirect the main page on Port 9000. Lastly, all the actions will be recorded on Port 5432 which is default port of PostgreSQL.

Application is created with Vue.js since it is easy to manage and ability to write html and javascript at the same time. Server is Node.js Express server which is widely using by software engineers in the industry. Finally, as a database PostgreSQL database has been selected.

For configuration of the application, docker compose is used since it is easy to configure. It created docker images regarding opening the port and PostgreSQL port on 5432 itself. It helped to maange configuration of FusionAuth.

OAuth allows access tokens (securtiy credentials) to issued third-part clients such as Google, Github, Facebook, Twitter with the approval of the source owner.

//// Purpose

The main goal of this application Implement OAuth 2.0 Authentication. Since authentication its getting more and more important, Oath 2.0 is a crucial topic to interest in since it provides authorization flows on web applications.

/// Functionality

This application will be an alternative to login with social auth which includes login with Google account, Facebook Account, Twitter and so on. This application is using OAuth 2.0 authentication layer, authentication policy standards which refers to Architecture for Authentication. 

//// Installation Instruction for a User

After cloning the application initially install npm dependencies for both server file and client file using with "npm install". Then, go through server file and type "npm run dev". Thus, 9000 port will be open if there is not an application that runs at that port. Morever, go through the client file and type "npm run serve". Finally, for both client and server side will be opened. After typing "localhost:8081 on the web page it will be seen that FusionAuth login credentianls page.

//// Test Results

For both sides including server and client were successfull. User can login FusionAuth via using Vue.js application and see the "Welcome The App" JSON response object. Moreover, the actions are recorded from PostgreSQL as it can be seen on Wireshark which is used as Packet Analyzer insturement. Test results can be available on slides.com presentation and observed more in detail.
